# Api Library

Library for using the internal API in various projects

## Graph api functions

### Get token
Get general that is used in graph to show you are authenticated for api calls.

### Get groups user
Return all the groups the user is connected to inside microsoft accounts.

### Check user in group
Send user email and group name return is a boolean inside success to let you know if the user is in group.

### add User(POST * required)
**userPrincipalName** | The user principal Name <br>
**givenName** | The given name of the new user <br>
**surname** | Surname of the user <br>

### add Meeting(POST * required)
**subject** | Subject of the meeting <br>
**userPrincipalName** | User where the meetings needs to be added <br>
**datum** | Datum of appointment <br>
**begin** | Start of meeting <br>
**eind** | End of meeting

### get Meeting(POST * required)
**userPrincipalName** | User principal names of the users to get the meetings from

## check Kalender (POST * required)
**kalendernaam** | Name of Calender you want to look at <br> **datum** | Date of appointment <br>
**begin** | Begin time of appointment time <br> **eind** | End date of the time you wan to look
## Vrijedagen Function

### Get all users
Returns all users inside vrijedagen

### Get user /{email}
Returns user based on email 

### Get all Sickdays
Returns all the registered sick days

### Get al Users vaction
Returns users with their vacation days inside 'vacantion' object

### Get all Current sickdays
Returns all the current people that are sick

### Get Sick Day Employee | /{Employeeid} 
Returns the sick day object of the employeeid

### Is user Sick | /{Employeeid}
Return success true or false if the user is sick

### get Clocked Im
Returns all the users that are currently clocked in

### get User Clocked in | /{Employeeid}
Returns boolean if user is clocked in

### get Administration Id
Returns freshdesk administration id

##freshDesk Functions 

### Create ticket(POST * required)
Hiermee kan je een ticket aanmaken zie required vars hieronder: <br>
**name** | Naam van ticket (string) <br> **subject** | Onderwerp ticket (string) <br> **description** | beschrijving ticket (string) <br>
**priority** | Prioriteit ticket 0/4 (int) <br> **status** | Welke status is de ticket 1/4 (int) <br> **type** | Is het een vraag etc (string) <br>
_email of phone moet aanwezig zijn_ <br>
**email** | Email van de persoon die de ticket aanmaakt (string) <br>
**phone** | Telefoon nummer van persoon die de ticket heeft aangemaakt (int) <br>
_responder_id moet aanwezig zijn of group id anders komt ticket in void_ <br>
**responder_id** | Id van agent die hier op moet reageren (int) <br>
**group_id** | Group id waar ticket in moet staan (int) <br>
_tags_ | een array met alle tags max 32 characters per string (array[(string)])

### groupsUser | /{email}
Gives back the groups the user is part of.

### get Tickets | /{email}
Gives back the tickets on the users name.

### get Group Tickets | /{groupName}/{email(optional)}
Returns back all tickets from group with option to search on certain email.

### change Group (POST) | /{mail}/{group(OPTIONEEL)}
Change the group of the user.

### get agent | /{email}
Based on email returns the user registered on the email inside freshdesk..

### get Group | /{groupname}
Based on name returns the group.

## Vestiging Function

### getAllVestingen
Gives back all the vestigingen.

### getAllWinkels
Gives back all the vestigingen that are stores.

### getVestiging | /vestigingsid
Give back specific vestiging with vestigingsid.

### getAlleOpeningstijden 
Returns all the openingstijden 

### getOpeningstijdenWinke; | /vestigingsid
returns openingstijden from specific store

### getKalender Naam | /{vestigingsid}/{soort}
Returns back name of the kalender used for these options.

### create Contact (POST * required)
**name** | Naam van contact (string) <br> **email** | Email van contact <br>
_phone_ | Telefoonnummer van contact 

### update Contact (POST * required fill in the var you want to change)
__name__ | Naam van contact (string) <br> __email__ | Email van contact <br>
_phone_ | Telefoonnummer van contact 

### delete Contact | /{contactID}
Delete the contact with this function

## Stats

### get Klanten Woonplaats 
Returns all the towns/cities clients live at.

### get Klanten totaal
Returns the total size of clients we have.

### get Transaction Day | /{from}/{till}
Returns all transactions day based.

### get Transaction Week | /{year}/{weekNumber}
Returns all transaction inside the week and year you send in the url/

### get Tranaction Month Day | /{year}/{month}/{day}
Returns all the transactions based on the year month and day.

## Printer

### get List Printers 
Returns list of all the printers inside the system.

### system Update
Update list of printers inside system.

### print Paper(POST * required)
Send a new print job in <br>
**Paper** | What kind of paper to use for the printing <br>
**location** | The location of where the prints needs to happen <br>
**filePath** | Path to the file that needs to printed

## Pin

### start Pin | /{locationid}/{factuurid}/{amount}
Start pin transaction with information given to it.

### cancel Pin | /{locationId}
Cancels current pin transaction based on that location

### refund Transation | /{locationId}/{factuurid}/{amount}/{userId}
Start a refund transaction.

## Mail

### Send(POST * required)
**vestigingsid** | Id from location that is needed <br> 
**fromMail** | Email adress from where the mail needs to be send <br>
**toMail** | Email addres to where the email needs to be send <br>
**subject** | The subject of the email <br>
**template** | The template that needs to be used from mail gun <br>
_body_ | Here you can add extra vars to the body and add them inside the lib to be send to mailgun

### Get template | /name 
Get more information about template with tag information.

## Bereik

### Call | /{van}/{vanvast}/{naar}  
Calls given number in get parameters.

## Feestdagen

### get All Feestdagen
Returns all dutch feestdagen.

## Afspraken

### Get alle soorten afspraken
Returns the different kind of afspraken.

### Get alle afspraken
Return all the afspraken.

### Get All Afspraken Vestiging | /{vestigingsid}
Returns all the afspraken based on vestigingsid.

### Get afspraken Dag | /{vestigingsid}/{datum}/{soort}
Returns all the afspraken based on date location and kind of afspraak.

### Get soort Afspraak | /{afspraakid}
Returns the row with afspraak data.

## Transparant

### Get Concullegas
Returns all the different places clients could have come from.

### Get Herkomst
Returns possibilities where clients could have learned about helder.

### create Klant  POST (*required)
**Email** | Email or phone number needs to be filled <br>
**Telefoonnummer**<br>
**Postcode**<br>
**Huisnummer**<br>
**Huisnummertoevoeging**<br>
**geboortedatum**<br>
**vestigingsid**<br>
