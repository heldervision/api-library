<?php


namespace Freshdesk;
use GuzzleHttp\Client;

class FreshdeskApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function groupUsers($email) {
        return $this->clientApi->get('/freshdesk/groupsUser/' . $email)->getBody()->getContents();
    }

    public function getTickets($email){
        return $this->clientApi->get('/freshdesk/tickets/' . $email)->getBody()->getContents();
    }

    public function getGroupTickets($groupname, $email = 'nice'){
        return $this->clientApi->get('/freshdesk/groupTickets/' . $groupname  . '/' . $email)->getBody()->getContents();
    }

    public function changeGroup($email, $group = 'nice'){
        return $this->clientApi->post('/freshdesk/changeGroup/' . $email . '/' . $group)->getBody()->getContents();
    }

    public function getAgent($email){
        return $this->clientApi->get('/freshdesk/getAgent/' . $email)->getBody()->getContents();
    }

    public function getGroup($groupName){
        return $this->clientApi->get('/freshdesk/getGroup/' . $groupName)->getBody()->getContents();
    }

    public function createTicket($name, $subject, $description, $priority, $status, $type, $email = '', $phone = '', $responderId = null, $groupId = null, $tags = null){
        $postVar = ['name' => $name, 'subject' => $subject, 'description' => $description, 'priority' => $priority, 'status' => $status, 'type' => $type, 'email' => $email, 'phone' => $phone];

        if (isset($responderId)) {
            $postVar['responder_id'] = $responderId;
        }

        if (isset($groupId)) {
            $postVar['group_id'] = intval($groupId);
        }

        if (isset($tags)) {
            $postVar['tags'] = serialize($tags);
        }

         return $this->clientApi->post('/freshdesk/createTicket', ['form_params' => $postVar])->getBody()->getContents();
    }

    public function createContact($name, $email, $phone = null){
        $postVar = ['name' => $name, 'email' => $email];

        if (isset($phone)){
            $postVar['phone'] = $phone;
        }

        return $this->clientApi->post('/freshdesk/createContact', ['form_params' => $postVar])->getBody()->getContents();
    }

    public function updateContact($contactId, $name = null, $email = null, $phone = null, $postBody = array()){
        if (isset($name)){
            $postBody['name'] = $name;
        }
        if (isset($email)){
            $postBody['email'] = $email;
        }
        if (isset($phone)){
            $postBody['phone'] = $phone;
        }

        return $this->clientApi->post("/freshdesk/updateContact/$contactId", ['form_params' => $postBody])->getBody()->getContents();
    }

    public function deleteContact($contactId){
        return $this->clientApi->get("/freshdesk/deleteContact/$contactId")->getBody()->getContents();
    }
}