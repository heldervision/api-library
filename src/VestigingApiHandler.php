<?php


namespace Vestigingen;


use GuzzleHttp\Client;

class VestigingApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function getAllVestingen(){
        return $this->clientApi->request('GET', '/vestigingen/getAll')->getBody()->getContents();
    }

    public function getAllWinkels(){
        return $this->clientApi->request('GET', '/vestigingen/getAllWinkels')->getBody()->getContents();
    }
    
    public function getVestiging($vestigingsId){
        return $this->clientApi->request('GET', '/vestigingen/getVestiging/' . $vestigingsId)->getBody()->getContents();
    }

    public function getAllOpeningstijden(){
        return $this->clientApi->request('GET', '/vestigingen/getAllOpeningstijden')->getBody()->getContents();
    }

    public function getOpeningsTijdWinkel($vestigingsid){
        return $this->clientApi->request('GET', "/vestigingen/getOpeningstijdWinkel/{$vestigingsid}")->getBody()->getContents();
    }

    public function getKalenderName($vestigingsid, $soort){
        return $this->clientApi->request('GET', "/vestigingen/getKalenderName/{$vestigingsid}/{$soort}")->getBody()->getContents();
    }
}