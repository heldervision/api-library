<?php


namespace HelderApi;
use GuzzleHttp\Client;


class PinApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function startPin($locationId, $factuurid, $amount){
        return $this->clientApi->get('/pin/start/' . $locationId . '/' . $factuurid . '/' . $amount)->getBody()->getContents();
    }

    public function cancelPin($locationId){
        return $this->clientApi->get('/pin/cancel/' . $locationId)->getBody()->getContents();
    }

    public function refundTransation($locationId, $factuurid, $amount, $userId){
        return $this->clientApi->get('/pin/refund/' .$locationId . '/' . $factuurid . '/' . $amount . '/' . $userId)->getBody()->getContents();
    }

    public function getReceipt($referral){
        return $this->clientApi->get('/pin/bon/' . $referral)->getBody()->getContents();
    }

}