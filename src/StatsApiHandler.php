<?php


namespace stats;
use GuzzleHttp\Client;


class StatsApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function getKlantenWoonplaats(){
        return $this->clientApi->get('/stats/klanten/woonplaats')->getBody()->getContents();
    }

    public function getKlantenTotaal(){
        return $this->clientApi->get('/stats/klanten/totaal')->getBody()->getContents();
    }

    public function getTransactionsDay($from, $till){
        return $this->clientApi->get('/stats/transacties/dag/' . $from . '/' . $till)->getBody()->getContents();
    }

    public function getTransactionWeek($year, $week){
        return $this->clientApi->get('/stats/transacties/totaal/' . $year .'/week-' . $week)->getBody()->getContents();
    }

    public function getTransactionMonthDay($year, $month, $day){
        return $this->clientApi->get('/stats/transacties/totaal/' . $year . '/' . $month . '/' . $day)->getBody()->getContents();
    }

}