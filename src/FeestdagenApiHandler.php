<?php


namespace Feestdagen;
use GuzzleHttp\Client;


class FeestdagenApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }
    // comment added
    public function getAllFeestdagen(){
        return $this->clientApi->get('/feestdagen/getFeestdagen')->getBody()->getContents();
    }
}