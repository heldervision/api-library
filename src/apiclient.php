<?php


namespace HelderApi;

require_once  __DIR__ .  '/../vendor/autoload.php';
require_once 'VrijedagenHandler.php';
require_once 'GraphApiHandler.php';
require_once 'VestigingApiHandler.php';
require_once 'FreshdeskApiHandler.php';
require_once 'BereikApiHandler.php';
require_once 'StatsApiHandler.php';
require_once 'PrintApiHandler.php';
require_once 'PinApiHandler.php';
require_once 'MailApiHandler.php';
require_once 'AfsprakenApiHandler.php';
require_once 'FeestdagenApiHandler.php';
require_once 'TransparantApiHandler.php';
require_once 'PostcodeApiHandler.php';

use AfsprakenHandler\AfsprakenApiHandler;
use Bereik\BereikApiHandler;
use Feestdagen\FeestdagenApiHandler;
use Freshdesk\FreshdeskApiHandler;
use GuzzleHttp\Client;
use mail\MailApiHandler;
use printer\PrintApiHandler;
use stats\StatsApiHandler;
use Validator\GraphApiHandler;
use Vestigingen\VestigingApiHandler;
use Vrijedagen\VrijedagenHandler;
use Transparant\TransparantApiHandler;
use Postcode\PostcodeApiHandler;


class apiclient
{
    private $privateGuzzleClient;
    public $graphApiHandler;
    public $vrijedagenHandler;
    public $vestigingenHandler;
    public $freshdeskHandler;
    public $bereikHandler;
    public $statsHandler;
    public $printerHandlder;
    public $pinHandler;
    public $mailHandler;
    public $afsprakenHandler;
    public $feestdageHandler;
    public $transparantHandler;
    public $postcodeHandler;

    public function __construct($apikey, $url)
    {
        $this->privateGuzzleClient = new Client(['base_uri' => $url,
            'headers' => [
                'Authorization' => $apikey
            ]
        ]);

        // Add handlers here
        $this->graphApiHandler = new GraphApiHandler($this->privateGuzzleClient);
        $this->vrijedagenHandler = new VrijedagenHandler($this->privateGuzzleClient);
        $this->vestigingenHandler = new VestigingApiHandler($this->privateGuzzleClient);
        $this->freshdeskHandler = new FreshdeskApiHandler($this->privateGuzzleClient);
        $this->bereikHandler = new BereikApiHandler($this->privateGuzzleClient);
        $this->statsHandler = new StatsApiHandler($this->privateGuzzleClient);
        $this->printerHandlder = new PrintApiHandler($this->privateGuzzleClient);
        $this->pinHandler = new PinApiHandler($this->privateGuzzleClient);
        $this->mailHandler = new MailApiHandler($this->privateGuzzleClient);
        $this->afsprakenHandler = new AfsprakenApiHandler($this->privateGuzzleClient);
        $this->feestdageHandler = new FeestdagenApiHandler($this->privateGuzzleClient);
        $this->transparantHandler = new TransparantApiHandler($this->privateGuzzleClient);
        $this->postcodeHandler = new PostcodeApiHandler($this->privateGuzzleClient);
    }
}