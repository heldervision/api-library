<?php


namespace printer;
use GuzzleHttp\Client;


class PrintApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function getListPrinter(){
        $this->clientApi->get('/print/system/list')->getBody()->getContents();
    }

    public function systemUpdate(){
        $this->clientApi->get('/print/system/update')->getBody()->getContents();
    }

    public function printPaper($location, $paper, $filePath){
        $this->clientApi->post('/print/new', [
            'form_params' => [
                'location' => $location,
                'paper' => $paper,
                'file' => $filePath
            ]
        ])->getBody()->getContents();
    }

}