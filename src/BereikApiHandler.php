<?php


namespace Bereik;
use GuzzleHttp\Client;


class BereikApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function call($van, $vanvast, $naar){
        return $this->clientApi->get('/bereik/call/' . $van . '/' . $vanvast . '/' . $naar)->getBody()->getContents();
    }
}