<?php


namespace Transparant;
use GuzzleHttp\Client;

class TransparantApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function getConcullegas(){
        return $this->clientApi->get('/transparant/getAllConcullegas')->getBody()->getContents();
    }

    public function getHerkomst(){
        return $this->clientApi->get('/transparant/getAllHerkomsten')->getBody()->getContents();
    }

    public function createKlant($email, $telefoonnummer, $postcode, $huisnummer, $huisnummertoevoegingen, $geboortedatum, $vestigingsid){
        return $this->clientApi->post('/transparant/createKlant', [
            'form_params' => ['email' => $email, 'telefoonnummer' => $telefoonnummer, 'postcode' => $postcode, 'huisnummer' => $huisnummer, 'geboortedatum' => $geboortedatum, 'huisnummertoevoeging' => $huisnummertoevoegingen, 'vestigingsid' => $vestigingsid]
        ])->getBody()->getContents();
    }

    public function getKlant($postcode, $huisnummer, $huisnummertoevoeging, $geboortedatum){
        return $this->clientApi->post('/transparant/getKlantId', [
            'form_params' => ['postcode' => $postcode, 'huisnummer' => $huisnummer, 'huisnummertoevoeging' => $huisnummertoevoeging, 'geboortedatum' => $geboortedatum]
        ])->getBody()->getContents();
    }
}