<?php


namespace AfsprakenHandler;


use GuzzleHttp\Client;

class AfsprakenApiHandler
{

    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function getAllSoortAfSpraken(){
        return $this->clientApi->get('/afspraak/getAllSoortAfspraken')->getBody()->getContents();
    }

    public function getAllAfspraken(){
        return $this->clientApi->get('/afspraak/getAllAfspraken')->getBody()->getContents();
    }

    public function getAfsprakenVestigingen($vestigingsid){
        return $this->clientApi->get('/afspraak/getAfsprakenVestiging/' . $vestigingsid)->getBody()->getContents();
    }

    public function getAfsprakenVandaag($vestigingsid, $datum, $type){
        return $this->clientApi->get('/afspraak/getAfsprakenVandaag/' . $vestigingsid . '/' . $datum . '/' . $type)->getBody()->getContents();
    }

    public function getSoortAfspraak($afspraakid){
        return $this->clientApi->get("/afspraak/getSoortAfspraak/{$afspraakid}")->getBody()->getContents();
    }

    public function getToekomstigeAfspraken($vestigingId){
        return $this->clientApi->get("/afspraak/getAlleToekomstigeAfspraken/{$vestigingId}")->getBody()->getContents();
    }
}