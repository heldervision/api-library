<?php


namespace Postcode;
use GuzzleHttp\Client;

class PostcodeApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function checkPostCode($postcode, $huisnummer, $huisnummertoevoeging = ''){
        return $this->clientApi->post('/postcode/checkPostcode', [
            'form_params' => [
                'postcode' => $postcode,
                'huisnummer' => $huisnummer,
                'huisnummertoevoeging' => $huisnummertoevoeging
            ]
        ])->getBody()->getContents();
    }

}