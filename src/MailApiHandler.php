<?php


namespace mail;
use GuzzleHttp\Client;

class MailApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function sendMail($locationid, $fromMail, $toMail, $subject, $template, $toName, $body = null, $userId = null){
        return $this->clientApi->post('/mail/send', [
            'form_params' => [
                'vestigingsid' => $locationid,
                'from' => $fromMail,
                'to' => $toMail,
                'subject' => $subject,
                'template' => $template,
                'toName' => $toName,
                'body' => $body
            ]
            // There can be added more vars in the future
        ])->getBody()->getContents();
    }

    public function getTemplate($nameTemplate){
        return $this->clientApi->get('/mail/getTemplate/' . $nameTemplate)->getBody()->getContents();
    }

}