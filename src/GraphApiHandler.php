<?php


namespace Validator;
use GuzzleHttp\Client;

class GraphApiHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function getToken(){
        return $this->clientApi->request('GET' , '/graph/getToken')->getBody()->getContents();
    }

    public function getGroupUser($email){
        return $this->clientApi->request('GET', '/graph/getGroupUser/'. $email)->getBody()->getContents();
    }

    public function isUserInGroup($email, $groupName){
        return $this->clientApi->request('GET', '/graph/userInGroup/'. $email.'/' . $groupName)->getBody()->getContents();
    }

    public  function addUser($userPrincipalName, $givenName, $surname){
        return $this->clientApi->post('/graph/addUser', [
            'form_params' => [
                'userPrincipalName' => $userPrincipalName,
                'givenName' => $givenName,
                'surname' => $surname
            ]
        ])->getBody()->getContents();
    }

    public function addMeeting($subject, $userPrincipalName, $datum, $begin, $eind, $body = ''){
        return $this->clientApi->post('/graph/addMeeting', [
            'form_params' => [
                'datum' => $datum,
                'reden' => $subject,
                'begin' => $begin,
                'eind' => $eind,
                'kalender' => $userPrincipalName,
                'body' => $body,
            ]
        ])->getBody()->getContents();
    }

    public function getMeetings($userPrincipalName){
        return $this->clientApi->post('/graph/getMeetings', [
            'form_params' => [
                'userPrincipalName' => $userPrincipalName
            ]
        ])->getBody()->getContents();
    }

    public function checkTime($kalenderNaam, $datum, $begin, $eind){
        return $this->clientApi->post('/graph/checkCalender', [
            'form_params' => [
                'kalender' => $kalenderNaam,
                'datum' => $datum,
                'begin' => $begin,
                'eind' => $eind,
            ]
        ])->getBody()->getContents();
    }

    public function getEventsOfDay($vestigingsId, $datum, $soort){
        return $this->clientApi->get("/graph/getAfsprakenVandaag/$vestigingsId/$datum/$soort")->getBody()->getContents();
    }
}