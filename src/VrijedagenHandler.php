<?php


namespace Vrijedagen;
use GuzzleHttp\Client;


class VrijedagenHandler
{
    private $clientApi;

    public function __construct(Client $clientApi)
    {
        $this->clientApi = $clientApi;
    }

    public function getClockedIn(){
        return $this->clientApi->get('/vrijedagen/wieisbinnen')->getBody()->getContents();
    }

    public function getUserClockedIn($userId){
        return $this->clientApi->get('/vrijedagen/getuserclockedin/' . $userId)->getBody()->getContents();
    }

    public function getAdminstartionId(){
        return $this->clientApi->get('/vrijedagen/adminstartionid')->getBody()->getContents();
    }


    public function getAllUsers(){
        return $this->clientApi->request('GET', '/vrijedagen/getAllUsers')->getBody()->getContents();
    }

    public function getAllSickDays(){
        return $this->clientApi->request('GET', '/vrijedagen/getAllSickDays')->getBody()->getContents();
    }

    public function getAllCurrentSickUsers(){
        return $this->clientApi->request('GET', '/vrijedagen/getAllCurrentSickUsers')->getBody()->getContents();
    }

    public function getSickDayEmployee($employeeId){
        return $this->clientApi->request('GET', '/vrijedagen/getSickDay/'. $employeeId)->getBody()->getContents();
    }

    public function isUserSick($employeeId){
        return $this->clientApi->request('GET', '/vrijedagen/isUserSick/'. $employeeId)->getBody()->getContents();
    }

    public function getAllUserVacation(){
        return $this->clientApi->request('GET', '/vrijedagen/getAllUsersVacation')->getBody()->getContents();
    }

    public function getUser($email){
        return $this->clientApi->request('GET', '/vrijedagen/getUserMail/' . $email)->getBody()->getContents();
    }
}